import React from 'react';
import {Provider} from "react-redux";
import thunkMiddleware from 'redux-thunk';
import {applyMiddleware, createStore} from "redux";

import Application from "./src/Containers/Application";
import reducer from "./src/store/reducer/reducer";

const store = createStore(reducer, applyMiddleware(thunkMiddleware));

export default class App extends React.Component {

  render() {
    return (
      <Provider store={store}>
        <Application/>
      </Provider>
    );
  }
}
