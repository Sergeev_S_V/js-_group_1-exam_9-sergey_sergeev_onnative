import React, {Component} from 'react';
import {Button, FlatList, Image, Modal, StyleSheet, Text, View} from "react-native";
import {connect} from "react-redux";
import {getContacts, getDetailsContact} from "../store/actions/contacts";
import Contact from "../Components/Contact/Contact";

class Application extends Component {

  state = {
    modalVisible: false,
  };

  toggleModal = (item) => {
    this.setState(prevState => {
      return {modalVisible: !prevState.modalVisible}
    });
    if (item) {
      this.props.getDetailsContact(item)
    }
  };

  componentDidMount() {
    this.props.getContacts();
  }

  render() {
    return(
      <View style={styles.container}>
        <Text style={styles.title}>Contacts</Text>
        <Modal visible={this.state.modalVisible}
               onRequestClose={() => this.toggleModal()}>
          <View>
            <View style={styles.placeItem}>
              <Text>{this.props.onPressedContact.name}</Text>
              <Image resizeMode="contain" source={{uri: this.props.onPressedContact.photo}} style={styles.image} />
              <Text>{this.props.onPressedContact.phone}</Text>
              <Text>{this.props.onPressedContact.email}</Text>
            </View>
            <Button title='Back to list' onPress={this.toggleModal}/>
          </View>
        </Modal>
        <FlatList
          style={styles.itemsList}
          keyExtractor={(item, index) => index}
          data={Object.keys(this.props.contacts)}
          renderItem={({item}) => (
            <Contact
              placeName={this.props.contacts[item].name}
              placeImage={this.props.contacts[item].photo}
              clicked={() => this.toggleModal(item)}
            />
          )}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  itemsList: {
    width: '100%',
  },
  title: {
    marginTop: 25,
    marginBottom: 10,
    fontSize: 20,
    fontWeight: 'bold'
  },
  image: {
    width: 50,
    height: 50,
    marginRight: 10
  },
});

const mapStateToProps = state => {
  return {
    contacts: state.contacts,
    onPressedContact: state.onPressedContact,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    getContacts: () => dispatch(getContacts()),
    getDetailsContact: key => dispatch(getDetailsContact(key)),
  }
};
export default connect(mapStateToProps ,mapDispatchToProps)(Application);