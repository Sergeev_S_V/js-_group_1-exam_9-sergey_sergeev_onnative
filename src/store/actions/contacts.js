import * as actionTypes from './actionTypes';
import axios from '../../axios-exam';


export const request = () => {
  return {type: actionTypes.REQUEST};
};

export const successResponseGetContacts = contacts => {
  return {type: actionTypes.SUCCESS_RESPONSE_GET_CONTACTS, contacts};
};

export const getContacts = () => dispatch => {
  dispatch(request());
  axios.get(`/contacts.json`)
    .then(contacts => contacts
      ? dispatch(successResponseGetContacts(contacts.data))
      : null
    )
};

export const getDetailsContact = key => dispatch => {
  dispatch(request());
  axios.get(`/contacts/${key}.json`)
    .then(contact => contact
      ? dispatch(successResponseDetailsContact(contact.data))
      : null
    )
};

export const successResponseDetailsContact = contact => {
  return {type: actionTypes.SUCCESS_RESPONSE_DETAILS_CONTACT, contact};
};
