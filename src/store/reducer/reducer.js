import * as actionTypes from '../actions/actionTypes';

const initialState = {
  contacts: {},
  onPressedContact: {},
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SUCCESS_RESPONSE_GET_CONTACTS:
      return {...state, contacts: action.contacts};
    case actionTypes.SUCCESS_RESPONSE_DETAILS_CONTACT:
      return {...state, onPressedContact: action.contact};
    default:
      return state;
  }

};

export default reducer;