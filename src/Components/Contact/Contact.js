import React from 'react';
import {Text, View, StyleSheet, TouchableOpacity, Image} from "react-native";

const Contact = props => (
  <TouchableOpacity onPress={props.clicked}>
    <View style={styles.placeItem}>
      <Image resizeMode="contain" source={{uri: props.placeImage}} style={styles.image} />
      <Text>{props.placeName}</Text>
    </View>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  placeItem: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    backgroundColor: '#eee',
    marginBottom: 10,
    padding: 10
  },
  image: {
    width: 50,
    height: 50,
    marginRight: 10
  }
});

export default Contact;